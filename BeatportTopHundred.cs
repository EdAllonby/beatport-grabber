﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace BeatportGrabber
{
    public partial class BeatportTopHundred : Form
    {
        private readonly BeatportGetter beatportGetter = new BeatportGetter();

        private IEnumerable<Artist> artists = new List<Artist>();

        public BeatportTopHundred()
        {
            InitializeComponent();
        }

        private event DataCompleteHandler DataComplete;

        private void GetData(object sender, EventArgs e)
        {
            var thread = new Thread(GetData);
            thread.Start();
            DataComplete += DataFetched;
            dataQueryProgressBar.Style = ProgressBarStyle.Marquee;
            dataQueryProgressBar.MarqueeAnimationSpeed = 30;
            getData.Enabled = false;
        }

        private void DataFetched()
        {
            Invoke((MethodInvoker) delegate
            {
                dataQueryProgressBar.Style = ProgressBarStyle.Continuous;

                getData.Enabled = true;
                var binder = new BindingSource {DataSource = artists};

                topHundredDataGrid.DataSource = binder;
            });
        }

        private void GetData()
        {
            artists = beatportGetter.Grab(20);
            DataComplete();
        }

        private delegate void DataCompleteHandler();
    }
}