﻿namespace BeatportGrabber
{
    partial class BeatportTopHundred
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.getData = new System.Windows.Forms.Button();
            this.topHundredDataGrid = new System.Windows.Forms.DataGridView();
            this.dataQueryProgressBar = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.topHundredDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // getData
            // 
            this.getData.Location = new System.Drawing.Point(330, 12);
            this.getData.Name = "getData";
            this.getData.Size = new System.Drawing.Size(75, 23);
            this.getData.TabIndex = 0;
            this.getData.Text = "Get Data";
            this.getData.UseVisualStyleBackColor = true;
            this.getData.Click += new System.EventHandler(this.GetData);
            // 
            // topHundredDataGrid
            // 
            this.topHundredDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.topHundredDataGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.topHundredDataGrid.Location = new System.Drawing.Point(0, 82);
            this.topHundredDataGrid.Name = "topHundredDataGrid";
            this.topHundredDataGrid.Size = new System.Drawing.Size(801, 429);
            this.topHundredDataGrid.TabIndex = 1;
            // 
            // dataQueryProgressBar
            // 
            this.dataQueryProgressBar.Enabled = false;
            this.dataQueryProgressBar.Location = new System.Drawing.Point(412, 12);
            this.dataQueryProgressBar.Name = "dataQueryProgressBar";
            this.dataQueryProgressBar.Size = new System.Drawing.Size(100, 23);
            this.dataQueryProgressBar.TabIndex = 2;
            // 
            // BeatportTopHundred
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 511);
            this.Controls.Add(this.dataQueryProgressBar);
            this.Controls.Add(this.topHundredDataGrid);
            this.Controls.Add(this.getData);
            this.Name = "BeatportTopHundred";
            this.Text = "BeatportTopHundred";
            ((System.ComponentModel.ISupportInitialize)(this.topHundredDataGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button getData;
        private System.Windows.Forms.DataGridView topHundredDataGrid;
        private System.Windows.Forms.ProgressBar dataQueryProgressBar;
    }
}

