﻿using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace BeatportGrabber
{
    /// <summary>
    /// Gets the Top 100 artists chart from Beatport
    /// And parses this data to Artist objects
    /// </summary>
    public sealed class BeatportGetter
    {
        /// <summary>
        /// Gets the Beatport Top-100 list and parses these into a collection of <see cref="Artist"/>s.
        /// </summary>
        /// <returns>A collection of <see cref="Artist"/>sfrom the Beatport Top-100 list.</returns>
        public IEnumerable<Artist> Grab(int rowsCount)
        {
            HtmlDocument document = CreateHtmlDocument();

            IEnumerable<string> tableInformation = ParseInformation(document);

            IEnumerable<Artist> artists = CreateArtists(tableInformation.ToList(), rowsCount);

            return artists;
        }

        private static HtmlDocument CreateHtmlDocument()
        {
            const string Link = "http://www.beatport.com/top-100";
            var web = new HtmlWeb();
            HtmlDocument document = web.Load(Link);
            return document;
        }

        private IEnumerable<Artist> CreateArtists(List<string> artistInformationCollection, int rows)
        {
            var artists = new List<Artist>();

            int rowCounter = 0;
            const int ColumnCount = 9;

            do
            {
                List<string> chartRow = artistInformationCollection.GetRange(rowCounter, ColumnCount);

                Artist chartArtist = CreateArtist(chartRow);

                artists.Add(chartArtist);

                rowCounter = rowCounter + ColumnCount;
            } while (rowCounter < rows*ColumnCount);

            return artists;
        }

        private static Artist CreateArtist(IReadOnlyList<string> row)
        {
            int chartPosition = int.Parse(row[0]);
            string trackName = row[2];
            string artist = row[3];
            string remixers = row[4];
            string labels = row[5];
            string genre = row[6];
            string releaseDate = row[7];
            string price = row[8];

            var chartArtist = new Artist(chartPosition, trackName, artist, remixers, labels, genre, releaseDate, price);
            return chartArtist;
        }

        private static IEnumerable<string> ParseInformation(HtmlDocument document)
        {
            const string NewLineDelimiter = "\n";
            const string TableXPath = "//*[@id=\"body\"]/div/div[2]/table";

            var dataList = new List<string>();

            foreach (HtmlNode row in document.DocumentNode.SelectNodes(TableXPath))
            {
                foreach (HtmlNode rowItem in row.SelectNodes("tr"))
                {
                    foreach (HtmlNode column in rowItem.SelectNodes("td"))
                    {
                        if (!column.InnerText.StartsWith(NewLineDelimiter))
                        {
                            string data = column.InnerText;

                            dataList.Add(column.InnerText.Contains(NewLineDelimiter)
                                ? data.Replace(NewLineDelimiter, "").Replace(" ", "").Replace("Buy", "")
                                : data);
                        }
                    }
                }
            }

            // Remove title bar information at top of table.
            dataList.RemoveRange(0, 9);

            return dataList;
        }
    }
}