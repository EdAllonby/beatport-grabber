﻿namespace BeatportGrabber
{
    public sealed class Artist
    {
        private readonly string artistName;
        private readonly int chartPosition;
        private readonly string genre;
        private readonly string labels;
        private readonly string price;
        private readonly string releaseDate;
        private readonly string remixers;
        private readonly string trackName;

        public Artist(int chartPosition, string artistName, string trackName, string remixers, string labels, string genre, string releaseDate, string price)
        {
            this.chartPosition = chartPosition;
            this.artistName = artistName;
            this.trackName = trackName;
            this.remixers = remixers;
            this.labels = labels;
            this.genre = genre;
            this.releaseDate = releaseDate;
            this.price = price;
        }

        public int ChartPosition
        {
            get { return chartPosition; }
        }

        public string ArtistName
        {
            get { return artistName; }
        }

        public string TrackName
        {
            get { return trackName; }
        }

        public string Remixers
        {
            get { return remixers; }
        }

        public string Labels
        {
            get { return labels; }
        }

        public string Genre
        {
            get { return genre; }
        }

        public string ReleaseDate
        {
            get { return releaseDate; }
        }

        public string Price
        {
            get { return price; }
        }
    }
}